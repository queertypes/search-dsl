{-# LANGUAGE OverloadedStrings #-}
module Unit.Parser (
  parserTests
) where

import Data.Attoparsec.Text (parseOnly)
import Data.Either
import Test.Tasty
import Test.Tasty.HUnit
import qualified Data.Attoparsec.Text as A

import Data.Search.Language
import Data.Search.Parser

parseVarMatchTests :: [TestTree]
parseVarMatchTests =
  let parsed = parseOnly parseVar
  in [ testCase "date" $ parsed "date" @?= Right "date"
     , testCase "dat3" $ parsed "dat3" @?= Right "dat3"
     , testCase "c0nn3t10n" $ parsed "c0nn3t10n" @?= Right "c0nn3t10n"
     ]

parseVarFailTests :: [TestTree]
parseVarFailTests =
  let parsed = parseOnly parseVar
  in [ testCase "string literal: \"date\"" $ assert (isLeft (parsed "\"date\""))
     , testCase "starts with num: 1d" $ assert (isLeft (parsed "\"date\""))
     , testCase "non-alphanum: u@me" $ assert (
         A.compareResults (A.parse parseVar "u@me") (A.Done "@me" "u") == Just True)
     ]

parseOpMatchTests :: [TestTree]
parseOpMatchTests =
  let parsed = parseOnly parseOp
  in  [ testCase "<" $ parsed "<" @?= Right Lt
      , testCase "<=" $ parsed "<=" @?= Right Lte
      , testCase ">" $ parsed ">" @?= Right Gt
      , testCase ">=" $ parsed ">=" @?= Right Gte
      , testCase "==" $ parsed "==" @?= Right Eq
      ]

parseOpFailTests :: [TestTree]
parseOpFailTests =
  let parsed = parseOnly parseOp
  in  [ testCase "=" $ assert (isLeft (parsed "="))
      , testCase "!=" $ assert (isLeft (parsed "!="))
      ]

parseLiteralMatchTests :: [TestTree]
parseLiteralMatchTests =
  let parsed = parseOnly parseLiteral
  in [ testCase "10" $ parsed "10" @?= Right (IntLit 10)
     , testCase "true" $ parsed "true" @?= Right (BoolLit True)
     , testCase "false" $ parsed "false" @?= Right (BoolLit False)
     , testCase "string: \"cat\"" $ parsed "\"cat\"" @?= Right (StringLit "cat")
     ]

parseLiteralFailTests :: [TestTree]
parseLiteralFailTests =
  let parsed = parseOnly parseLiteral
  in [ testCase "matching var: cat" $ assert (isLeft (parsed "cat"))
     , testCase "True" $ assert (isLeft (parsed "True"))
     , testCase "False" $ assert (isLeft (parsed "False"))
     , testCase "-1" $ assert (isLeft (parsed "-1"))
     ]

parseBinOpMatchTests :: [TestTree]
parseBinOpMatchTests =
  let parsed = parseOnly parseBinOp
  in [ testCase "date < 1" $ parsed "date < 1" @?= Right (BinOpL Lt "date" (IntLit 1))
     , testCase "1 < date" $ parsed "1 < date" @?= Right (BinOpR Lt (IntLit 1) "date")
     , testCase "date < 1 != 1 < date" $ assert (parsed "1 < date" /= parsed "date < 1")
     ]

parseBinOpFailTests :: [TestTree]
parseBinOpFailTests =
  let parsed = parseOnly parseBinOp
  in [ testCase "space matters: 1<date" $ assert (isLeft (parsed "1<date"))
     , testCase "space matters: date<1" $ assert (isLeft (parsed "date<1"))
     , testCase "missing var: 1 == 1" $ assert (isLeft (parsed "1 == 1"))
     , testCase "missing lit: date == date" $ assert (isLeft (parsed "date == date"))
     , testCase "bad var: 1 < u@me" (
         assert (isLeft (parsed "u@me")))
     ]

parseExprMatchTests :: [TestTree]
parseExprMatchTests =
  let parsed = parseOnly parseExpr
  in [ testCase "date < 1" $ parsed "date < 1" @?= Right (BinOpL Lt "date" (IntLit 1))
     , testCase "0 < date and date < 1" (
         parsed "0 < date and date < 1" @?=
         Right (And (BinOpR Lt (IntLit 0) "date") (BinOpL Lt "date" (IntLit 1))))
     ]

parseExprFailTests :: [TestTree]
parseExprFailTests =
  let parsed = parseOnly parseExpr
  in [ testCase "missing and: 0 < date 1 > date" (
         assert (isLeft (parsed "0 < date 1 > date" )))
     , testCase "missing exprs: and" (
         assert (isLeft (parsed "and")))
     , testCase "missing space: 1<dateanddate<1" (
         assert (isLeft (parsed "1<dateanddate<1")))
     , testCase "incomplete: 1 < date and date >" (
         assert (isLeft (parsed "1 < date and date >")))
     , testCase "incomplete: 1 < date and 1 >" (
         assert (isLeft (parsed "1 < date and 1 >")))
     , testCase "incomplete: 1 < date and true" (
         assert (isLeft (parsed "1 < date and true")))
     , testCase "bad var: 1 < u@me" (
         assert (isLeft (parsed "1 < u@me")))
     ]

parserTests :: TestTree
parserTests = testGroup "parser"
  [ testGroup "var:match" parseVarMatchTests
  , testGroup "var:fail" parseVarFailTests
  , testGroup "op:match" parseOpMatchTests
  , testGroup "op:fail" parseOpFailTests
  , testGroup "literal:match" parseLiteralMatchTests
  , testGroup "literal:fail" parseLiteralFailTests
  , testGroup "binaryOp:match" parseBinOpMatchTests
  , testGroup "binaryOp:fail" parseBinOpFailTests
  , testGroup "expr:match" parseExprMatchTests
  , testGroup "expr:fail" parseExprFailTests
  ]
