module Data.Search.Eval.ElasticSearch (
  eval
) where

import Database.Bloodhound (Filter)

import Data.Search.Language

eval :: Env -> Expr -> Filter
eval env (And e1 e2) = undefined
eval env (BinOpL e1 e2 e3) = undefined
eval env (BinOpR e1 e2 e3) = undefined
