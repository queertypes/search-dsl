module Data.Search.Eval (
  isTypeError,
  isUnknownFieldError,
  isOperationNotSupportedError,
  eval,
  can,
  lookup,
  Error(..)
) where

import Prelude hiding (lookup)

import Data.List ((\\))
import Data.Text (Text, unpack)
import qualified Prelude as P

import Data.Search.Language


data Error
  = UnknownField Text
  | TypeMismatch Type Literal Var
  | OperationNotSupported [Supports] Type Var

isTypeError :: Error -> Bool
isTypeError (TypeMismatch _ _ _) = True
isTypeError _ = False

isUnknownFieldError :: Error -> Bool
isUnknownFieldError (UnknownField _) = True
isUnknownFieldError _ = False

isOperationNotSupportedError :: Error -> Bool
isOperationNotSupportedError (OperationNotSupported _ _ _) = True
isOperationNotSupportedError _ = False

instance Show Error where
  show (UnknownField v) = "error: unknown field " ++ unpack v
  show (TypeMismatch t l v) =
    "error: expecting " ++ show t ++ " (from: " ++ unpack v ++ ") but found " ++ show (typeOf l)
  show (OperationNotSupported missing t v) =
    "error: (" ++ unpack v ++ " : " ++ show t ++ ") does not support " ++ show missing

{-
Evaulation steps:

1. [lookup] Lookup a given var in the env. If found, remember its type
2. [ts:class] Check that the type supports the expected operation
3. [ts:lit] Check that the type of the literal is the same as the var
4. Generate the corresponding string

-}
eval :: Env -> Expr -> Either Error String
eval env (And e1 e2) = do
  e1' <- eval env e1
  e2' <- eval env e2
  return (e1' ++ " and " ++ e2')
eval env (BinOpL op v l) =
  case lookup v env of
     Nothing -> Left (UnknownField v)
     (Just t) -> check v t l op
eval env (BinOpR op l v) =
  case lookup v env of
     Nothing -> Left (UnknownField v)
     (Just t) -> check v t l op

typeOf :: Literal -> Type
typeOf (IntLit _) = IntType
typeOf (StringLit _) = StringType
typeOf (BoolLit _) = BoolType

check :: Var -> Type -> Literal -> Op -> Either Error String
check v t l op
  | not (t `can` op) =
      Left (OperationNotSupported (needs op \\ supports t) t v)
  | t /= typeOf l =
      Left (TypeMismatch t l v)
  | otherwise = Right (unpack v ++ " " ++ show op ++ " " ++ show l)

lookup :: Var -> Env -> Maybe Type
lookup = P.lookup

needs :: Op -> [Supports]
needs Lt = [Ordering]
needs Gt = [Ordering]
needs Eq = [Equality]
needs Gte = [Ordering, Equality]
needs Lte = [Ordering, Equality]

supports :: Type -> [Supports]
supports IntType = [Ordering, Equality]
supports StringType = [Ordering, Equality]
supports BoolType = [Equality]

can :: Type -> Op -> Bool
can t op = null (needs op \\ supports t)
