{-# LANGUAGE OverloadedStrings #-}
module Data.Search.Parser (
  parseVar,
  parseLiteral,
  parseOp,
  parseBinOp,
  parseExpr,
  parse
) where

import Prelude hiding (takeWhile)

import Control.Applicative
import Data.Attoparsec.Text hiding (parse)
import Data.Functor
import Data.Monoid
import Data.Text (Text, singleton)

import Data.Search.Language

--------------------------------------------------------------------------------
                           -- Interface --
--------------------------------------------------------------------------------
parseLiteral :: Parser Literal
parseOp      :: Parser Op
parseBinOp   :: Parser Expr
parseExpr    :: Parser Expr
parseVar     :: Parser Var
parse        :: Text -> Either String Expr

--------------------------------------------------------------------------------
                         -- Implementation --
--------------------------------------------------------------------------------
parseVar = ((<>) <$> (singleton <$> letter) <*> (takeWhile (inClass "a-zA-Z0-9")))

parseLiteral =
  (StringLit <$> ("\"" *> takeWhile (/= '"') <* "\""))
  <|> (IntLit <$> decimal)
  <|> (BoolLit <$> ("true" $> True))
  <|> (BoolLit <$> ("false" $> False))


parseOp =
  ("==" $> Eq)
  <|> (">=" $> Gte)
  <|> ("<=" $> Lte)
  <|> (">" $> Gt)
  <|> ("<" $> Lt)


parseBinOp = varOnLeft <|> varOnRight
  where varOnLeft = do
          var <- parseVar
          op <- spaced parseOp
          lit <- parseLiteral
          return (BinOpL op var lit)

        varOnRight = do
          lit <- parseLiteral
          op <- spaced parseOp
          var <- parseVar
          return (BinOpR op lit var)


parseExpr = parseAnd <|> (parseBinOp <* endOfInput)
  where parseAnd = do
          e1 <- parseBinOp
          void (spaced "and")
          And <$> pure e1 <*> parseExpr

parse = parseOnly parseExpr

spaced :: Parser a -> Parser a
spaced f = space *> skipSpace *> f <* space <* skipSpace
