# Search DSL

This project aims to be a tutorial-level reference on how to craft a
search DSL. Such a search DSL might serve as a component in an API
that allows users to access selected data from a backing store (*SQL,
ElasticSearch, etc.).

## Setup

### Testing

* All tests

```bash
$ stack test
```

* All evaluation tests

```bash
$ stack test search:unit --test-arguments '-p eval'
```

* All parsing tests

```bash
$ stack test search:unit --test-arguments 'p parser'
```

### Build

* If you don't have stack: get
  [stack](http://docs.haskellstack.org/en/stable/README.html#how-to-install)
* If you don't have a Haskell compiler: `stack setup`

* Otherwise:

```bash
$ stack build
```

* Enter a REPL session:

```bash
$ stack ghci
```

## Examples

Here's a session showing parsing.

```haskell
> :set -XOverloadedStrings
> import Data.Attoparsec (parseOnly)
> parseOnly parseExpr "date < \"2010-10-12\""
Right (BinOpL < "date" (Str 2010-10-12))
> parseOnly parseExpr "\"2010-10-12\" < date"
Right (BinOpR < (Str 2010-10-12) "date")
> parseOnly parseExpr "\"2010-10-12\" < date and count > 5"
Right (And (BinOpR < (Str 2010-10-12) "date") (BinOpL > "count" (Int 5)))
```

Here's some evaluation. This amounts to generating a similar string or
reporting a semantic error. There's a default environment provided,
`env`:

```haskell
-- successful case
> eval env <$> parse "\"2010-10-12\" < date and count > 5"
Right (Right "date < (Str 2010-10-12) and count > (Int 5)")

-- name error: "dat" not found
> eval env <$> parse "\"2010-10-12\" < dat and count > 5"
Right (Left error: unknown field dat)

-- type error: int != bool
> eval env <$> parse "\"2010-10-12\" < date and count > true"
Right (Left error: expecting int (from: count) but found bool)

-- unsupported op: bool does not implement Ordering
> eval env <$> parse "\"2010-10-12\" < date and confidential > true"
Right (Left error: (confidential : bool) does not support [Ordering])
```
